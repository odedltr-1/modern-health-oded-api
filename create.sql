-- MODERN HEALTH DB CREATE --

-- Declare the scheme and add 'pgcrypto' extension.
DROP SCHEMA IF EXISTS modern_health_schema CASCADE;
DROP EXTENSION IF EXISTS pgcrypto;
CREATE SCHEMA modern_health_schema;
CREATE EXTENSION pgcrypto WITH SCHEMA modern_health_schema;

-- Declare a function to auto generate random string records IDs of 8 chars length.
-- This might be a bit more wasteful.
-- However, will prevent a hacker from predicting our records IDs.
CREATE OR REPLACE FUNCTION modern_health_schema.generate_uid() RETURNS TEXT AS $$
  DECLARE size INT := 8;
  DECLARE
    characters TEXT := 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    bytes BYTEA := modern_health_schema.gen_random_bytes(size);
    l INT := length(characters);
    i INT := 0;
    output TEXT := '';
  BEGIN
    WHILE i < size LOOP
	  output := output || substr(characters, get_byte(bytes, i) % l + 1, 1);
	  i := i + 1;
	END LOOP;
    RETURN output;
  END;
$$ LANGUAGE plpgsql VOLATILE;

-- Create 'program' table and populate.
CREATE TABLE modern_health_schema.program (
  id CHARACTER(8) PRIMARY KEY DEFAULT modern_health_schema.generate_uid(),
  name VARCHAR(64) NOT NULL UNIQUE CHECK (name <> ''),
  description VARCHAR(128)
);
INSERT INTO modern_health_schema.program (id, name, description) VALUES
('p_123456', 'Leadership Development Program', 'Leadership Development Program description...');
INSERT INTO modern_health_schema.program (name, description) VALUES
('Cognitive Behavioral Therapy', 'Cognitive Behavioral Therapy...'),
('New Parenting', 'New Parenting description...'),
('Mindful Communication', 'Mindful Communication description...');

-- Create 'section' table and populate.
CREATE TABLE modern_health_schema.section (
  id CHARACTER(8) PRIMARY KEY DEFAULT modern_health_schema.generate_uid(),
  program_id CHARACTER(8) NOT NULL REFERENCES modern_health_schema.program,
  name VARCHAR(64) NOT NULL UNIQUE CHECK (name <> ''),
  description VARCHAR(128),
  image_path TEXT,
  order_index SMALLINT NOT NULL
);
INSERT INTO modern_health_schema.section (program_id, name, description, image_path, order_index) VALUES
('p_123456', 'Leadership Development Section1', 'Leadership Development Section1 desc...', 'path/to/img1', 5),
('p_123456', 'Leadership Development Section2', 'Leadership Development Section2 desc...', 'path/to/img2', 1);

-- Create 'user' table and populate.
CREATE TABLE modern_health_schema.user (
  id CHARACTER(8) PRIMARY KEY DEFAULT modern_health_schema.generate_uid(),
  email VARCHAR(32) NOT NULL UNIQUE CHECK (email <> ''),
  f_name VARCHAR(32) NOT NULL UNIQUE CHECK (f_name <> ''),
  l_name VARCHAR(32) NOT NULL UNIQUE CHECK (l_name <> '')
);
INSERT INTO modern_health_schema.user (id, email, f_name, l_name) VALUES
('1W1CFXkJ', 'odedltr@gmail.com', 'Oded', 'Alter'),
('10203040', 'odedltr1@gmail.com', 'Test', 'User');

-- Create 'access_roles’ table and populate.
-- Access roles is an enum of the roles defined in our API.
CREATE TABLE modern_health_schema.access_roles (
  role VARCHAR(32) PRIMARY KEY NOT NULL UNIQUE CHECK (role <> '')
);
INSERT INTO modern_health_schema.access_roles (role) VALUES
('PROGRAM_USER'), ('PROGRAM_ADMIN');

-- Create 'user_to_access_roles’ table and populate.
-- Shows what user has what access roles.
CREATE TABLE modern_health_schema.user_to_access_roles (
  user_id CHARACTER(8) NOT NULL REFERENCES modern_health_schema.user,
  role VARCHAR(32) NOT NULL REFERENCES modern_health_schema.access_roles
);
INSERT INTO modern_health_schema.user_to_access_roles (user_id, role) VALUES
('1W1CFXkJ', 'PROGRAM_USER'),
('1W1CFXkJ', 'PROGRAM_ADMIN'),
('10203040', 'PROGRAM_USER');





