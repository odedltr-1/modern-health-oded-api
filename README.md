# Oded's home assignment for Modern Health

This is a simple API server running on Typescript/Node/Express stack.

### NOTE: Marisa (recruiter) has the password for PostgreSQL. Please paste it in src/config.ts

## Table of Contents

- [Installation](#markdown-header-installation)
- [Usage](#markdown-header-usage)
- [Entities](#markdown-header-entities)
- [API](#markdown-header-api)
- [Architecture](#markdown-header-architecture)
- [Database](#markdown-header-database)
- [Security](#markdown-header-security)
- [Misc](#markdown-header-misc)

## Installation

After cloning you may checkout to any of the branches:

```bash
origin/master
origin/development
```

Install dependencies:

```bash
npm install
```

## Usage

For running locally (will run on port 3000):

```
npm run start
```

For running on a different port, for example 3001:

```
PORT=3001 npm run start
```

For cleaning & rebuilding:

```
npm run rebuild
```

Will delete 'dist' folder, rebuild and run.

NOTE: On building you might see the following error:

```
[nodemon] app crashed - waiting for file changes before starting...
```

It's because nodemon can't find the start file 'dist/server.js' since it hasn't been created yet, should be OK after a second.

## Entities

There are 3 main entities in this API:

1. Program
2. Section (nested under program)
3. User

## API

There are 2 endpoints groups exposed on this API

```
api_v1/program
api_v1/section
```

(The user operations are not exposed to end-users)

Each endpoints group allow all the CRUD ops allowed in a REST architecture.
To invoke, use relevant URL with the needed HTTP method and needed data or params.

## Architecture

For every exposed entity there is a corresponding N-tier flow;

### Routers

All the available endpoints to support CRUD ops, with the matching permissions (see security).

### Controllers

For each endpoint there is a corresponding controller action that trigger the business logic and handles the response to the client.

### Business Logic (service)

Gather the needed data before executing a DB CRUD action, also in some cases process the DB result prior to returning it to the controller.
Services may communicate with each other, so if entity 1 needs something from entity 2 that is the place.

### Data Access Layer (model)

Execute sanitized DB calls, this is the only point where the API interact with the DB.

## Database

- The database in use is PostgreSQL v 12, hosted on Google Cloud Platform.
- The ORM in use is npm 'pg' package, a PostgreSQL client for node.js https://github.com/brianc/node-postgres
- On the root folder there is a create.sql which can install the DB, tables and populate with data.
- pg.connect.helper is a singleton class that establishes the connection interacts with the DB.
- Configuration to connect are stored in config.ts, normally prod credentials like this would be in gitignore.

## Security

Every URL endpoint requires a role. On express.helper.ts a check is being done for the invoking user's roles.
Express middleware allows to the flow to continue to be handled by the controller, or to return 403 to the user.

## Misc

- The database instance should be open to the world (0.0.0.0/0), please do not share the user/pass access.
  Also let me know when I can remove the public access, upon you done examine the assignment.

- Missing; test cases and activity entity. My recruiter Marisa suggested that this exercise should take 2 hours.
  However building an API and designing a DB takes a bit longer when done correctly and I didn't want to loose too much time on this.
