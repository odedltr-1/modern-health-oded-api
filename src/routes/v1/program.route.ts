import { Express, Router } from 'express';

import ProgramController from '../../controllers/program.controller';
import * as exHelpers from '../../helpers/express.helper';

const BASE_ROUTE = '/api_v1/program';

/**
 * Endpoints for PROGRAM operations.
 *
 * @export
 * @class ProgramRouter
 */
export default class ProgramRouter {
  constructor(app: Express) {
    const router = Router();

    // [GET] Returns a list of all the programs.
    // PERM: PROGRAM_USER
    router.get('/', exHelpers.validateUser('PROGRAM_USER'), exHelpers.asyncMiddleware(ProgramController.listAll));

    // [GET] Returns a single program by its ID.
    // PERM: PROGRAM_USER
    router.get('/:programID', exHelpers.validateUser('PROGRAM_USER'), exHelpers.asyncMiddleware(ProgramController.getRecordByID));

    // [POST] Creates a new program and return the newly created document with its ID.
    // PERM: PROGRAM_ADMIN
    // Request Body: please view interface IProgram.
    router.post('/', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(ProgramController.addRecord));

    // [PUT] Patch a program and return the updated document with its ID.
    // PERM: PROGRAM_ADMIN
    // Request Body: please view interface IProgram.
    router.put('/', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(ProgramController.patchRecord));

    // [DELETE] Delete a program document by a given ID.
    // PERM: PROGRAM_ADMIN
    router.delete('/:programID', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(ProgramController.deleteRecord));

    app.use(BASE_ROUTE, router);
  }
}
