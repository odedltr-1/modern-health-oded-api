import { Express, Router } from 'express';

import SectionController from '../../controllers/section.controller';
import * as exHelpers from '../../helpers/express.helper';

const BASE_ROUTE = '/api_v1/section';

/**
 * Endpoints for SECTION operations.
 *
 * @export
 * @class SectionRouter
 */
export default class SectionRouter {
  constructor(app: Express) {
    const router = Router();

    // [GET] Returns a list of all the sections by a program ID.
    // PERM: PROGRAM_USER
    router.get('/program/:programID', exHelpers.validateUser('PROGRAM_USER'), exHelpers.asyncMiddleware(SectionController.listAll));

    // [GET] Returns a single SECTION by its ID.
    // PERM: PROGRAM_USER
    router.get('/:sectionID', exHelpers.validateUser('PROGRAM_USER'), exHelpers.asyncMiddleware(SectionController.getRecordByID));

    // [POST] Add a new SECTION to a PROGRAM and return the newly created document with its ID.
    // PERM: PROGRAM_ADMIN
    // Request Body: please view interface ISection.
    router.post('/program/:programID', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(SectionController.addRecord));

    // [PUT] Patch a SECTION and return the updated document with its ID.
    // PERM: PROGRAM_ADMIN
    // Request Body: please view interface ISection.
    router.put('/', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(SectionController.patchRecord));

    // [DELETE] Delete a SECTION document by a given ID.
    // PERM: PROGRAM_ADMIN
    router.delete('/:sectionID', exHelpers.validateUser('PROGRAM_ADMIN'), exHelpers.asyncMiddleware(SectionController.deleteRecord));

    app.use(BASE_ROUTE, router);
  }
}
