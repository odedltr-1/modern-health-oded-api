import { Pool, PoolClient, QueryResult } from 'pg';

import config from '../config';

export default class PG_ConnectHelper {
  private static instance: PG_ConnectHelper | null;
  private pool = new Pool(config.postgresAccount);

  private constructor() {
    console.log(`Postgres server:`, config.postgresAccount.host);

    this.pool.on('connect', (client: PoolClient) => {
      console.log('Postgres server connected');
    });

    this.pool.on('error', (err: Error, client: PoolClient) => {
      console.log('Postgres server error:', err.message);
    });
  }

  private static getInstance() {
    if (!PG_ConnectHelper.instance) PG_ConnectHelper.instance = new PG_ConnectHelper();
    return PG_ConnectHelper.instance.pool;
  }

  /**
   * Runs the query and returns the matching rows.
   *
   * @static
   * @param {string} query
   * @param {Array<any>} [values]
   * @returns {Promise<Array<any>>}
   * @memberof PG_ConnectHelper
   */
  static async queryRows(query: string, values?: Array<any>): Promise<Array<any>> {
    return PG_ConnectHelper.query(query, values).then((res) => {
      return res.rows;
    });
  }

  /**
   * Runs the query and returns a single matching row.
   *
   * @static
   * @param {string} query
   * @param {Array<any>} [values]
   * @returns {Promise<any>}
   * @memberof PG_ConnectHelper
   */
  static async querySingleLine(query: string, values?: Array<any>): Promise<any> {
    return PG_ConnectHelper.query(query, values).then((res) => {
      if (res.rows.length != 1) throw 'NOT_FOUND';
      return res.rows[0];
    });
  }

  /**
   * Runs any query and returns as a Promise.
   *
   * @static
   * @param {string} query
   * @param {Array<any>} [values]
   * @returns {Promise<QueryResult>}
   * @memberof PG_ConnectHelper
   */
  static async query(query: string, values?: Array<any>): Promise<QueryResult> {
    const pool = PG_ConnectHelper.getInstance();
    // console.log('running query', pool.totalCount); // prints the pool size.
    return pool.query(query, values);
  }
}
