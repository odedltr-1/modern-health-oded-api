import { NextFunction, Request, Response } from 'express';

import { IUser } from '../interfaces/user.interface';
import UserService from '../business-logic/user.service';

type USER_ROLES = 'PROGRAM_ADMIN' | 'PROGRAM_USER';

/**
 * Express middleware that validates user's roles for the requested end point against the roles the user has in the DB.
 *
 * @param {USER_ROLES} userRoles Requested role for the API endpoint, should be provided by the router.
 * @returns {Promise<any>}
 */
export const validateUser = function validateUser(userRole: USER_ROLES) {
  const validateUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (req && req.body && req.body && req.body.userID) {
        const user: IUser = await UserService.getRecordByID(req.body.userID);
        if (user && Array.isArray(user.roles) && user.roles.includes(userRole)) return next();
      }
      return res.status(403).send('Unauthorized');
    } catch (errorMSG) {
      errorHandler(errorMSG, res);
    }
  };
  return validateUser;
};

/**
 * Async express middleware wrapper
 *
 * https://medium.com/@Abazhenov/using-async-await-in-express-with-node-8-b8af872c0016
 *
 * @param {*} fn
 */
export const asyncMiddleware = (fn: any) =>
  function syncMiddlewareWrap(...args: any) {
    return Promise.resolve(fn(...args)).catch(args[args.length - 1]);
  };

export const errorHandler = function errorHandler(errorFlag: string | any, res: Response) {
  console.log('Error:', errorFlag);

  switch (errorFlag) {
    case 'BAD_REQUEST':
      return res.status(400).send('Bad request or missing params.');
    case 'CANNOT_COMPLETE_REQUEST':
      return res.status(409).send('Your request cannot be completed.');
    case 'NOT_FOUND':
      return res.status(404).send('Record not found.');
    default:
      return res.status(500).send('Internal Server Error.');
  }
};
