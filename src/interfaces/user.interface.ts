import { USER_ROLES } from './../config';

export interface IUser {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  roles: Array<USER_ROLES>;
}
