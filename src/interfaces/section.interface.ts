export interface ISection {
  id: string;
  name: string;
  description: string;
  imagePath: string;
  orderIndex: number;
  programID: string;
}
