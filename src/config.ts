import { PoolConfig } from 'pg';

export default class Config {
  static port: number = Number(process.env.PORT) || 3000;
  static userRoles: Array<string> = ['PROGRAM_USER', 'PROGRAM_ADMIN'];

  // Normally this will be in gitignore.
  static postgresAccount: PoolConfig = {
    host: '34.69.142.81',
    database: 'modern-health-stage-db',
    user: 'modern-health-stage-user',
    password: 'ASK-MARISA-FOR-THE-PASS',
    max: 10
  };
}

export type USER_ROLES = 'PROGRAM_USER' | 'PROGRAM_ADMIN';
