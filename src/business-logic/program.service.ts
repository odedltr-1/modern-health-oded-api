import ProgramModel from '../data-access-layer/program.model';
import { IProgram } from '../interfaces/program.interface';

export default class ProgramService {
  /**
   * List all PROGRAM records.
   *
   * @static
   * @returns {Promise<Array<any>>}
   * @memberof ProgramService
   */
  static async listAll(): Promise<Array<IProgram>> {
    return ProgramModel.listAll();
  }

  /**
   * Get a single PROGRAM record by ID.
   *
   * @static
   * @param {string} recordID The record ID of needed document.
   * @returns {Promise<any>}
   * @memberof ProgramService
   */
  static async getRecordByID(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    return ProgramModel.getRecordByID(recordID);
  }

  /**
   * Creates an new PROGRAM and return the newly created document with its ID.
   *
   * @static
   * @param {(IProgram | any)} record The new record to be added.
   * @returns {(Promise<IProgram>)}
   * @memberof ProgramService
   */
  static async addRecord(record: IProgram | any): Promise<IProgram> {
    if (!record) throw 'BAD_REQUEST';

    // Inserting only allowed values, ignoring unrecognized values.
    const newRecord: any = {};
    for (const key of ['name', 'description']) newRecord[key] = record[key] || null;

    const recordID = await ProgramModel.addRecord(newRecord).then((result) => {
      if (result && Array.isArray(result.rows) && result.rows[0] && result.rows[0].id) return result.rows[0].id;
    });

    if (recordID) return ProgramService.getRecordByID(recordID);
    else throw 'CANNOT_COMPLETE_REQUEST';
  }

  /**
   * Update a PROGRAM record.
   *
   * @static
   * @param {string} recordID The ID of the record to be patched.
   * @param {(IProgram | any)} update
   * @returns {(Promise<IProgram>)}
   * @memberof ProgramService
   */
  static async patchRecord(recordID: string, update: IProgram | any): Promise<IProgram> {
    if (!recordID || !update) throw 'BAD_REQUEST';

    const currentRecord = await ProgramModel.getRecordByID(recordID);

    // Inserting only allowed values, ignoring unrecognized values.
    // Keeping existing values that are not updated..
    const newRecord: any = {};
    for (const key of ['name', 'description']) {
      if (key in update) newRecord[key] = update[key];
      else newRecord[key] = currentRecord[key] || null;
    }

    await ProgramModel.updateRecord(recordID, newRecord);
    return ProgramService.getRecordByID(recordID);
  }

  /**
   * Delete a program document by a given ID.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<void>}
   * @memberof ProgramService
   */
  static async deleteRecord(recordID: string): Promise<void> {
    if (!recordID) throw 'BAD_REQUEST';
    await ProgramModel.deleteRecord(recordID);
  }
}
