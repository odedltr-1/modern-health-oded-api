import UserModel from '../data-access-layer/user.model';
import { IUser } from '../interfaces/user.interface';

export default class UserService {
  /**
   * Get a single USER record by ID, including user's roles.
   * Throw 'NOT_FOUND' if no matching record.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<IUser>}
   * @memberof UserService
   */
  static async getRecordByID(recordID: string): Promise<IUser> {
    if (!recordID) throw 'BAD_REQUEST';
    return UserModel.getRecordByID(recordID).then((records) => {
      if (Array.isArray(records) && records.length)
        return {
          id: records[0].id,
          email: records[0].email,
          firstName: records[0].f_name,
          lastName: records[0].l_name,
          roles: records.map((x: any) => x.role)
        };
      throw 'NOT_FOUND';
    });
  }
}
