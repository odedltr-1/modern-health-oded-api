import SectionModel from '../data-access-layer/section.model';
import { ISection } from '../interfaces/section.interface';

export default class SectionService {
  /**
   * List all SECTION records by a give program ID.
   *
   * @static
   * @param {string} programID The ID of the program to list sections by.
   * @returns {Promise<Array<ISection>>}
   * @memberof SectionService
   */
  static async listAll(programID: string): Promise<Array<ISection>> {
    if (!programID) throw 'BAD_REQUEST';
    const sections = await SectionModel.listAll(programID);
    return sections;
  }

  /**
   * Get a single SECTION record by ID.
   *
   * @static
   * @param {string} recordID The record ID of needed document.
   * @returns {Promise<any>}
   * @memberof SectionService
   */
  static async getRecordByID(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    return SectionModel.getRecordByID(recordID).then((record) => {
      return {
        id: record.id,
        programID: record.program_id,
        name: record.name,
        description: record.description,
        imagePath: record.image_path,
        orderIndex: record.order_index
      };
    });
  }

  /**
   * Creates an new SECTION and return the newly created document with its ID.
   *
   * @static
   * @param {string} programID ID of the program to add the section to.
   * @param {(ISection | any)} record The new record to be added.
   * @returns {Promise<ISection>}
   * @memberof SectionService
   */
  static async addRecord(programID: string, record: ISection | any): Promise<ISection> {
    if (!programID || !record) throw 'BAD_REQUEST';

    // Inserting only allowed values, ignoring unrecognized values.
    const newRecord: any = {};
    for (const key of ['name', 'description', 'imagePath', 'orderIndex']) newRecord[key] = record[key] || null;

    const recordID = await SectionModel.addRecord(programID, newRecord).then((result) => {
      if (result && Array.isArray(result.rows) && result.rows[0] && result.rows[0].id) return result.rows[0].id;
    });

    if (recordID) return SectionModel.getRecordByID(recordID);
    else throw 'CANNOT_COMPLETE_REQUEST';
  }

  /**
   * Update a SECTION record.
   *
   * @static
   * @param {string} recordID The ID of the record to be patched.
   * @param {(ISection | any)} update
   * @returns {Promise<ISection>}
   * @memberof SectionService
   */
  static async patchRecord(recordID: string, update: ISection | any): Promise<ISection> {
    if (!recordID || !update) throw 'BAD_REQUEST';

    const currentRecord = await SectionService.getRecordByID(recordID);

    // Inserting only allowed values, ignoring unrecognized values.
    // Keeping existing values that are not updated.
    const newRecord: any = {};
    for (const key of ['name', 'description', 'imagePath', 'orderIndex', 'programID']) {
      if (key in update) newRecord[key] = update[key];
      else newRecord[key] = currentRecord[key] || null;
    }

    await SectionModel.updateRecord(recordID, newRecord);
    return SectionModel.getRecordByID(recordID);
  }

  /**
   * Delete a SECTION document by a given ID.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<void>}
   * @memberof SectionService
   */
  static async deleteRecord(recordID: string): Promise<void> {
    if (!recordID) throw 'BAD_REQUEST';
    await SectionModel.deleteRecord(recordID);
  }
}
