import { NextFunction, Request, Response } from 'express';

import ProgramService from '../business-logic/program.service';
import * as exHelpers from '../helpers/express.helper';
import { IProgram } from '../interfaces/program.interface';

export default class ProgramController {
  /**
   * List all PROGRAM records.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof ProgramController
   */
  static async listAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    const programs: Array<IProgram> = await ProgramService.listAll();
    res.status(200).json({ programs });
  }

  /**
   * Get a single PROGRAM record by ID.
   * Sends 400 if not supplied.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof ProgramController
   */
  static async getRecordByID(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const programID = req.params.programID;
      if (programID) {
        const program = await ProgramService.getRecordByID(programID);
        res.status(200).json({ program });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Creates an new program and return the newly created document with its ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof ProgramController
   */
  static async addRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const reqBody = req.body;
      if (reqBody) {
        const program = await ProgramService.addRecord(reqBody);
        res.status(200).json({ program });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Patch a program and return the updated document with its ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof ProgramController
   */
  static async patchRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const reqBody = req.body;
      if (reqBody && reqBody.id) {
        const program = await ProgramService.patchRecord(reqBody.id, reqBody);
        res.status(200).json({ program });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Delete a program document by a given ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof ProgramController
   */
  static async deleteRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    const programID = req.params.programID;
    if (programID) {
      await ProgramService.deleteRecord(programID);
    } else res.status(400).send('Bad request or missing params.');

    res.status(200).json({ status: 'ok' });
  }
}
