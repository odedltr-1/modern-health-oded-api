import { NextFunction, Request, Response } from 'express';

import ProgramService from '../business-logic/program.service';
import SectionService from '../business-logic/section.service';
import * as exHelpers from '../helpers/express.helper';
import { ISection } from '../interfaces/section.interface';

export default class SectionController {
  /**
   * List all SECTION records by a given program ID.
   * Sends 400 if not supplied.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof SectionController
   */
  static async listAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    const programID = req.params.programID;
    try {
      if (programID) {
        const program = await ProgramService.getRecordByID(programID);

        // Making sure that program exists.
        if (program) {
          const sections: Array<ISection> = await SectionService.listAll(programID);
          res.status(200).json({ sections });
        } else throw 'NOT_FOUND';
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Get a single SECTION record by ID.
   * Sends 400 if not supplied.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof SectionController
   */
  static async getRecordByID(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const sectionID = req.params.sectionID;
      if (sectionID) {
        const section = await SectionService.getRecordByID(sectionID);
        res.status(200).json({ section });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Add a new SECTION to a PROGRAM and return the newly created document with its ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof SectionController
   */
  static async addRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const programID = req.params.programID;
      const reqBody = req.body;
      if (programID && reqBody) {
        const section = await SectionService.addRecord(programID, reqBody);
        res.status(200).json({ section });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Patch a SECTION and return the updated document with its ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof SectionController
   */
  static async patchRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const reqBody = req.body;
      if (reqBody && reqBody.id) {
        const section = await SectionService.patchRecord(reqBody.id, reqBody);
        res.status(200).json({ section });
      } else res.status(400).send('Bad request or missing params.');
    } catch (errorMSG) {
      exHelpers.errorHandler(errorMSG, res);
    }
  }

  /**
   * Delete a section document by a given ID.
   *
   * @static
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @returns {Promise<void>}
   * @memberof SectionController
   */
  static async deleteRecord(req: Request, res: Response, next: NextFunction): Promise<void> {
    const sectionID = req.params.sectionID;
    if (sectionID) {
      await SectionService.deleteRecord(sectionID);
    } else res.status(400).send('Bad request or missing params.');

    res.status(200).json({ status: 'ok' });
  }
}
