import express = require('express');
import * as bodyParser from 'body-parser';

import config from './config';

import * as ProgramRouter from './routes/v1/program.route';
import * as SectionRouter from './routes/v1/section.route';

const app = express(); // Create a new express app instance.

app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.send('Hello Modern Health !');
});

for (const route of [ProgramRouter, SectionRouter]) new route.default(app);

app.use((req: express.Request, res: express.Response, next: Function): void => {
  res.status(404).send('The requested endpoint does not exist');
});

app.listen(config.port, function () {
  console.log(`Modern Health app is listening on port ${config.port} !`);
});
