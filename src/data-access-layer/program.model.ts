import { IProgram } from 'interfaces/program.interface';
import PG_ConnectHelper from '../helpers/pg.connect.helper';

export default class ProgramModel {
  /**
   * List all PROGRAM records.
   *
   * @static
   * @returns {Promise<Array<any>>}
   * @memberof ProgramModel
   */
  static async listAll(): Promise<Array<any>> {
    const query = `SELECT * FROM modern_health_schema.program;`;
    return PG_ConnectHelper.queryRows(query, []);
  }

  /**
   * Get a single PROGRAM record by ID.
   * Throw 'NOT_FOUND' if no matching record.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<any>}
   * @memberof ProgramModel
   */
  static async getRecordByID(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    const query = `
      SELECT * FROM modern_health_schema.program
      WHERE id = $1;
    `;
    return PG_ConnectHelper.querySingleLine(query, [recordID]);
  }

  /**
   * Creates an new PROGRAM and return the newly created document with its ID.
   *
   * @static
   * @param {IProgram} record The new record to be added.
   * @returns {Promise<any>}
   * @memberof ProgramModel
   */
  static async addRecord(record: IProgram): Promise<any> {
    if (!record) throw 'BAD_REQUEST';
    const query = `
      INSERT INTO modern_health_schema.program (name, description) VALUES
      ($1, $2) 
      RETURNING id;
    `;
    return PG_ConnectHelper.query(query, [record.name, record.description]);
  }

  /**
   * Update a PROGRAM record.
   *
   * @static
   * @param {string} recordID The ID of the record to be updated.
   * @param {IProgram} record The record to be update.
   * @returns {Promise<any>}
   * @memberof ProgramModel
   */
  static async updateRecord(recordID: string, record: IProgram): Promise<any> {
    if (!record) throw 'BAD_REQUEST';
    const query = `
      UPDATE modern_health_schema.program 
      SET name = $1, description = $2
      WHERE id = $3;
    `;

    return PG_ConnectHelper.query(query, [record.name, record.description, recordID]);
  }

  /**
   * Delete a PROGRAM document by a given ID.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<any>}
   * @memberof ProgramModel
   */
  static async deleteRecord(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    const query = `
      DELETE FROM modern_health_schema.program 
      WHERE id = $1;
    `;
    return PG_ConnectHelper.query(query, [recordID]);
  }
}
