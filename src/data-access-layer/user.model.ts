import PG_ConnectHelper from '../helpers/pg.connect.helper';

export default class UserModel {
  /**
   * Get a single USER record by ID, including user's roles.
   *
   * @static
   * @param {string} recordID
   * @returns {Promise<any>}
   * @memberof UserModel
   */
  static async getRecordByID(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    const query = `
      SELECT modern_health_schema.user.*, modern_health_schema.user_to_access_roles.role
      FROM modern_health_schema.user JOIN modern_health_schema.user_to_access_roles
      ON modern_health_schema.user.id = $1
      AND modern_health_schema.user.id = user_to_access_roles.user_id;
    `;
    return PG_ConnectHelper.queryRows(query, [recordID]);
  }
}
