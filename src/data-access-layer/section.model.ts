import { ISection } from 'interfaces/section.interface';
import PG_ConnectHelper from '../helpers/pg.connect.helper';

export default class SectionModel {
  /**
   * List all SECTION records by a give program ID.
   *
   * @static
   * @param {string} programID The ID of the program to list sections by.
   * @returns {Promise<Array<any>>}
   * @memberof SectionModel
   */
  static async listAll(programID: string): Promise<Array<any>> {
    const query = `
      SELECT * FROM modern_health_schema.section
      WHERE program_id = $1
      ORDER BY order_index;
    `;
    return PG_ConnectHelper.queryRows(query, [programID]);
  }

  /**
   * Get a single SECTION record by ID.
   * Throw 'NOT_FOUND' if no matching record.
   *
   * @static
   * @param {string} recordID The record ID of needed document.
   * @returns {Promise<any>}
   * @memberof SectionModel
   */
  static async getRecordByID(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    const query = `
      SELECT * FROM modern_health_schema.section
      WHERE id = $1;
    `;
    return PG_ConnectHelper.querySingleLine(query, [recordID]);
  }

  /**
   * Creates an new SECTION and return the newly created document with its ID.
   *
   * @static
   * @param {string} programID ID of the program to add the section to.
   * @param {(ISection)} record The new record to be added.
   * @returns {Promise<any>}
   * @memberof SectionModel
   */
  static async addRecord(programID: string, record: ISection): Promise<any> {
    console.log('SectionModel', 'addRecord', 'programID', programID, 'record', record);
    if (!programID || !record) throw 'BAD_REQUEST';
    const query = `
      INSERT INTO modern_health_schema.section (program_id, name, description, image_path, order_index) VALUES
      ($1, $2, $3, $4, $5) 
      RETURNING id;
    `;
    return PG_ConnectHelper.query(query, [programID, record.name, record.description, record.imagePath, record.orderIndex]);
  }

  /**
   * Update a SECTION record.
   *
   * @static
   * @param {string} recordID The ID of the record to be updated.
   * @param {ISection} record The record to be update.
   * @returns {Promise<any>}
   * @memberof SectionModel
   */
  static async updateRecord(recordID: string, record: ISection): Promise<any> {
    console.log(recordID, record);

    if (!record) throw 'BAD_REQUEST';
    const query = `
      UPDATE modern_health_schema.section 
      SET program_id = $1, name = $2, description = $3, image_path = $4, order_index = $5
      WHERE id = $6;
    `;

    return PG_ConnectHelper.query(query, [
      record.programID,
      record.name,
      record.description,
      record.imagePath,
      record.orderIndex,
      recordID
    ]);
  }

  /**
   * Delete a SECTION record by a given ID.
   *
   * @static
   * @param {string} recordID ID of the record to be deleted.
   * @returns {Promise<any>}
   * @memberof SectionModel
   */
  static async deleteRecord(recordID: string): Promise<any> {
    if (!recordID) throw 'BAD_REQUEST';
    const query = `
      DELETE FROM modern_health_schema.section 
      WHERE id = $1;
    `;
    return PG_ConnectHelper.query(query, [recordID]);
  }
}
